package com.mall.promo.service;

import com.mall.promo.PromoService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

@Service(interfaceClass = PromoService.class)
@Component
public class PromoServiceImpl implements PromoService {

}

package com.mall.promo.dal.persistence;

import com.mall.commons.tool.tkmapper.TkMapper;
import com.mall.promo.dal.entitys.PromoItem;

public interface PromoItemMapper extends TkMapper<PromoItem> {
}

package com.mall.promo.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

import java.util.List;

@Data
public class SecKillListResponse extends AbstractResponse {

    private String code;

    private String msg;

    private List<> productList;

    private Integer psId;

    private Boolean sessionId;
}

package com.mall.promo.dto;

import com.mall.commons.result.AbstractRequest;
import lombok.Data;

@Data
public class SecKillListRequest extends AbstractRequest {

    private Integer sessionId;

    @Override
    public void requestCheck() {

    }
}

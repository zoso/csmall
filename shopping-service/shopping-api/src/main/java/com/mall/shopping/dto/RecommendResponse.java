package com.mall.shopping.dto;/**
 * Created by ciggar on 2019/7/29.
 */

import com.mall.commons.result.AbstractResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 *  ciggar
 * create-date: 2019/7/29-下午11:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecommendResponse extends AbstractResponse{

    private Set<PanelDto> panelContentItemDtos;

}

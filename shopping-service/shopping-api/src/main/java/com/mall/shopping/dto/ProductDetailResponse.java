package com.mall.shopping.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  ciggar
 * create-date: 2019/7/24-16:27
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetailResponse extends AbstractResponse {
    private ProductDetailDto productDetailDto;
}

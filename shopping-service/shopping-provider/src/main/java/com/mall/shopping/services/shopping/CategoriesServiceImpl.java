package com.mall.shopping.services.shopping;

import com.mall.shopping.IProductCateService;
import com.mall.shopping.IProductService;
import com.mall.shopping.converter.ProductCateConverter;
import com.mall.shopping.dal.entitys.ItemCat;
import com.mall.shopping.dal.persistence.ItemCatMapper;
import com.mall.shopping.dto.AllProductCateRequest;
import com.mall.shopping.dto.AllProductCateResponse;
import com.mall.shopping.dto.ProductCateDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service
@Component
@Slf4j
public class CategoriesServiceImpl implements IProductCateService {

    @Autowired
    ItemCatMapper itemCatMapper;

    @Autowired
    ProductCateConverter productCateConverter;

    @Override
    public AllProductCateResponse getAllProductCate(AllProductCateRequest request) {

        List<ItemCat> itemCats = itemCatMapper.selectAll();
        AllProductCateResponse allProductCateResponse = new AllProductCateResponse();
        List<ProductCateDto> productCateDtoList = productCateConverter.items2Dto(itemCats);
        allProductCateResponse.setProductCateDtoList(productCateDtoList);
        return allProductCateResponse;

        /*List<ProductCateDto> productCateDtoList = itemCatMapper.selectProductCate();
        AllProductCateResponse response = new AllProductCateResponse();
        response.setProductCateDtoList(productCateDtoList);

        return response;*/
    }
}

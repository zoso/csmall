package com.mall.shopping.services.shopping;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.shopping.IProductService;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.converter.ProductConverter;
import com.mall.shopping.converter.ProductIemConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.entitys.ItemDesc;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.*;
import com.mall.shopping.dto.*;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Service(interfaceClass = IProductService.class)
public class ProductServiceImpl implements IProductService {
    @Autowired
    private ItemMapper itemMapper;

    @Autowired
    private ItemCatMapper itemCatMapper;

    @Autowired
    private ItemDescMapper itemDescMapper;

    @Autowired
    private PanelMapper panelMapper;

    @Autowired
    private PanelContentMapper panelContentMapper;

    @Autowired
    private ProductIemConverter productIemConverter;

    @Autowired
    private ProductConverter productConverter;

    @Autowired
    private ContentConverter contentConverter;

    public ProductDetailResponse getProductDetail(ProductDetailRequest request) {
        Item item = itemMapper.selectByPrimaryKey(request.getId());
        String[] split = item.getImage().split(",");
        List<String> list = Arrays.asList(split);
        ItemDesc itemDesc = itemDescMapper.selectByPrimaryKey(request.getId());
        ProductDetailDto productDetailDto = productIemConverter.item2Dto(item);
        productDetailDto.setDetail(itemDesc.getItemDesc());
        productDetailDto.setProductImageSmall(list);
        productDetailDto.setProductImageBig(list.get(0));
        return new ProductDetailResponse(productDetailDto);
    }

    @Override
    public AllProductResponse getAllProduct(AllProductRequest request) {
        PageHelper.startPage(request.getPage(),request.getSize());
        Example example = new Example(Item.class);
        Example.Criteria criteria = example.createCriteria();
        if ("1".equals(request.getSort())){
            example.setOrderByClause("price asc");
        }
        if ("-1".equals(request.getSort())){
            example.setOrderByClause("price desc");
        }
        if (request.getCid() != null){
            criteria.andEqualTo("cid",request.getCid());
        }
        if (request.getPriceGt() != null){
            criteria.andGreaterThanOrEqualTo("price",request.getPriceGt());
        }
        if (request.getPriceLte() != null){
            criteria.andLessThanOrEqualTo("price",request.getPriceLte());
        }
        List<Item> items = itemMapper.selectByExample(example);
        int i = itemMapper.selectCountByExample(example);
        List<ProductDto> productDtos = productConverter.items2Dto(items);
        PageInfo<ProductDto> productDtoPageInfo = new PageInfo<>(productDtos);
        long total = productDtoPageInfo.getTotal();
        return new AllProductResponse(productDtos,Long.valueOf(i));
    }

    @Override
    public RecommendResponse getRecommendGoods() {
        Panel panel = panelMapper.selectByPrimaryKey(6);
        PanelDto panelDto = contentConverter.panen2Dto(panel);
        List<PanelContentItem> panelContentItems = panelContentMapper.selectPanelContentAndProductWithPanelId(6);
        List<PanelContentItemDto> panelContentItemDtos = contentConverter.panelContentItem2Dto(panelContentItems);
        panelDto.setPanelContentItems(panelContentItemDtos);
        Set<PanelDto> panelDtos = new HashSet<>();
        panelDtos.add(panelDto);
        return new RecommendResponse(panelDtos);
    }
}

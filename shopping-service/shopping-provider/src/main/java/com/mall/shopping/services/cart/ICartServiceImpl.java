package com.mall.shopping.services.cart;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.mall.shopping.ICartService;
import com.mall.shopping.converter.CartItemConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.*;
import org.apache.dubbo.config.annotation.Service;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@Component
public class ICartServiceImpl implements ICartService {

    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private ItemMapper itemMapper;

    //获得购物车商品列表
    @Override
    public List<CartProductDto> getCartListById(CartListByIdRequest request) {
        RList<Object> list = redissonClient.getList("user_cart:" + request.getUserId());
        List<Object> list1 = list.readAll();
        List<CartProductDto> cartProductDtos = new ArrayList<>();
        for (Object o : list1) {
            cartProductDtos.add(JSON.parseObject(o.toString(), CartProductDto.class));
        }

        return cartProductDtos;
    }

//    //添加商品到购物车
//    @Override
//    public void addToCart(AddCartRequest request) {
//        Item item = itemMapper.selectByPrimaryKey(request.getProductId());
//        CartProductDto cartProductDto = CartItemConverter.item2Dto(item);
//        cartProductDto.setProductNum(request.getProductNum().longValue());
//        cartProductDto.setChecked("true");
//        RList<Object> list = redissonClient.getList("user_cart:" + request.getUserId());
//        list.add(JSON.toJSON(cartProductDto).toString());
//    }

    //添加商品到购物车
    @Override
    public void addToCart(AddCartRequest request) {
        RList<Object> list = redissonClient.getList("user_cart:" + request.getUserId());
        Iterator<Object> iterator = list.iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            String next = (String) iterator.next();
            CartProductDto cartProductDto = JSON.parseObject(next, CartProductDto.class);
            if (cartProductDto.getProductId().equals(request.getProductId())) {
                cartProductDto.setProductNum(cartProductDto.getProductNum() + 1);
                list.set(i, JSON.toJSONString(cartProductDto));
                return;
            }
        }
        Item item = itemMapper.selectByPrimaryKey(request.getProductId());
        CartProductDto cartProductDto = CartItemConverter.item2Dto(item);
        cartProductDto.setProductNum(request.getProductNum().longValue());
        cartProductDto.setChecked("true");
        list.add(JSON.toJSON(cartProductDto).toString());
    }

    //更新购物车中商品的数量
    @Override
    public UpdateCartNumResponse updateCartNum(UpdateCartNumRequest request) {
        RList<Object> list = redissonClient.getList("user_cart:" + request.getUserId());
        Iterator<Object> iterator = list.iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            String next = (String) iterator.next();
            CartProductDto cartProductDto = JSON.parseObject(next, CartProductDto.class);
            if (cartProductDto.getProductId().equals(request.getProductId())) {
                cartProductDto.setProductNum(request.getProductNum().longValue());
                cartProductDto.setChecked(request.getChecked());
                list.set(i, JSON.toJSONString(cartProductDto));
                return new UpdateCartNumResponse();
            }
        }
        return new UpdateCartNumResponse();
    }

    //选择购物车中的所有商品
    @Override
    public CheckAllItemResponse checkAllCartItem(CheckAllItemRequest request) {
        RList<Object> list = redissonClient.getList("user_cart:" + request.getUserId());
        Iterator<Object> iterator = list.iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            String next = (String) iterator.next();
            CartProductDto cartProductDto = JSON.parseObject(next, CartProductDto.class);
            cartProductDto.setChecked(request.getChecked());
            list.set(i, JSON.toJSONString(cartProductDto));
        }
        return new CheckAllItemResponse();
    }


    //删除购物车中的商品
    @Override
    public void deleteCartItem(DeleteCartItemRequest request) {
        RList<Object> list = redissonClient.getList("user_cart:" + request.getUserId());
        Iterator<Object> iterator = list.iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            String next = (String) iterator.next();
            CartProductDto cartProductDto = JSON.parseObject(next, CartProductDto.class);
            if (cartProductDto.getProductId().equals(request.getItemId())) {
                iterator.remove();
            }
        }
    }

    //删除选中的商品
    @Override
    public DeleteCheckedItemResposne deleteCheckedItem(DeleteCheckedItemRequest request) {
        RList<Object> list = redissonClient.getList("user_cart:" + request.getUserId());
        Iterator<Object> iterator = list.iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            String next = (String) iterator.next();
            CartProductDto cartProductDto = JSON.parseObject(next, CartProductDto.class);
            if (cartProductDto.getChecked().equals("true")) {
                iterator.remove();
            }
        }
        return new DeleteCheckedItemResposne();
    }

    //清空指定用户的购物车缓存(用户下完订单之后清理）
    @Override
    public void clearCartItemByUserID(ClearCartItemRequest request) {
        RList<Object> list = redissonClient.getList("user_cart:" + request.getUserId());
        Iterator<Object> iterator = list.iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            String next = (String) iterator.next();
            CartProductDto cartProductDto = JSON.parseObject(next, CartProductDto.class);
            if (request.getProductIds().contains(cartProductDto.getProductId())) {
                iterator.remove();
            }
        }
    }

}

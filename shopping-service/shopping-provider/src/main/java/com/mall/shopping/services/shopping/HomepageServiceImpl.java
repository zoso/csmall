package com.mall.shopping.services.shopping;

import com.mall.shopping.IHomeService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContent;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dal.persistence.PanelMapper;
import com.mall.shopping.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.HashSet;
import java.util.List;

@Service
@Component
@Slf4j
public class HomepageServiceImpl implements IHomeService {

    @Autowired
    PanelMapper panelMapper;

    @Autowired
    ContentConverter contentConverter;

    @Override
    public HomePageResponse homepage() {
        HomePageResponse response = new HomePageResponse();
        List<Panel> panelList = panelMapper.selectAll();
        HashSet<PanelDto> dtoHashSet = new HashSet<>();
        for (Panel panel : panelList) {
            PanelDto panelDto = contentConverter.panen2Dto(panel);
            Integer id = panelDto.getId();
            List<PanelContentItem> panelContentItems = panelContentMapper.selectPanelContentAndProductWithPanelId(id);
            List<PanelContentItemDto> panelContentItemDtos = contentConverter.panelContentItem2Dto(panelContentItems);
            panelDto.setPanelContentItems(panelContentItemDtos);
            dtoHashSet.add(panelDto);
        }
        response.setCode(ShoppingRetCode.SUCCESS.getCode());
        response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        response.setPanelContentItemDtos(dtoHashSet);
        return response;
    }

    @Autowired
    PanelContentMapper panelContentMapper;

    @Override
    public NavListResponse shoppingNavigation() {
        Example example = new Example(PanelContent.class);
        example.createCriteria().andEqualTo("panelId", 0);
        List<PanelContent> panelContents = panelContentMapper.selectByExample(example);
        List<PanelContentDto> panelContentDtos = contentConverter.panelContents2Dto(panelContents);
        NavListResponse response = new NavListResponse();
        response.setPannelContentDtos(panelContentDtos);
        response.setCode(ShoppingRetCode.SUCCESS.getCode());
        response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        return response;
    }
}

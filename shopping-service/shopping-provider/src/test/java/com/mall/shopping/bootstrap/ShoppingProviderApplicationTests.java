package com.mall.shopping.bootstrap;

import com.mall.shopping.dto.*;
import com.mall.shopping.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShoppingProviderApplication.class)
public class ShoppingProviderApplicationTests {

    @Autowired
    private ICartService cartService;


    @Test
    public void testCartService() throws IOException {
        AddCartRequest request = new AddCartRequest();
        request.setProductNum(1);
        request.setProductId(100023501L);
        request.setUserId(123L);
        cartService.addToCart(request);
        System.in.read();
    }

}

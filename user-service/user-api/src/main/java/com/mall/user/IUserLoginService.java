package com.mall.user;

import com.mall.user.dto.CheckAuthRequest;
import com.mall.user.dto.CheckAuthResponse;
import com.mall.user.dto.UserLoginRequest;
import com.mall.user.dto.UserLoginResponse;

/**
 * @auther Albert-King
 * @date 2020/8/17 13:52
 */
public interface IUserLoginService {
	UserLoginResponse userLoginService(UserLoginRequest userLoginRequest);

	CheckAuthResponse validToken(CheckAuthRequest checkAuthRequest);

	boolean captchaConfirm(String captcha,String kaptchaUuid);
}

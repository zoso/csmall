package com.mall.user;

import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;

/**
 * @auther Albert-King
 * @date 2020/8/19 15:28
 */
public interface IUserVerifyService {
	UserVerifyResponse userVerify(UserVerifyRequest userVerifyRequest);
}

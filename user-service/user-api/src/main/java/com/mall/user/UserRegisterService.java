package com.mall.user;

import com.mall.user.dto.UserRegisterRequest;
import com.mall.user.dto.UserRegisterResponse;

/**
 * @auther Albert-King
 * @date 2020/8/18 6:11
 */
public interface UserRegisterService {
	UserRegisterResponse userRegister(UserRegisterRequest userRegisterRequest);

	boolean captchaConfirm(String captcha, String kaptchaUuid);
}

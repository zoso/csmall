package com.mall.user.services;

import com.mall.user.UserRegisterService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dal.entitys.UserVerify;
import com.mall.user.dal.persistence.UserRegisterMapper;
import com.mall.user.dto.UserLoginResponse;
import com.mall.user.dto.UserRegisterRequest;
import com.mall.user.dto.UserRegisterResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailMessage;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @auther Albert-King
 * @date 2020/8/18 6:11
 */
@Slf4j
@Component
@Service
public class UserRegisterServiceImpl implements UserRegisterService {
	@Autowired
	UserRegisterMapper userRegisterMapper;

	@Autowired
	RedissonClient redissonClient;
	private final String KAPTCHA_UUID="kaptcha_uuid";

	@Autowired
	MailSender mailSender;

	@Override
	public boolean captchaConfirm(String captcha,String kaptchaUuid) {
		/**
		 * 验证码实现逻辑
		 * 1、验证码存在于cookie中，存放的是验证码图片的文本数据和uuid
		 * 2、在redis中，验证码的uuid作为key，实际的验证码作为value，
		 * 需要取出redis中的验证码与前端传入的参数做对比
		 */
//		从redis中获取真实的验证码
		RBucket rBucket=redissonClient.getBucket(KAPTCHA_UUID+kaptchaUuid);
		String realCaptcha = (String) rBucket.get();
		if(captcha.equals(realCaptcha))
			return true;
		return false;
	}

	@Override
	public UserRegisterResponse userRegister(UserRegisterRequest userRegisterRequest) {
//		判空验证
		userRegisterRequest.requestCheck();

		String email = userRegisterRequest.getEmail();
		String userName = userRegisterRequest.getUserName();
		String password = userRegisterRequest.getUserPwd();
		List<UserRegisterResponse> userRegisterResponses=new ArrayList<>();
//		判断用户名是否存在
		userRegisterResponses=userRegisterMapper.selectUserByUserName(userName);
		if(userRegisterResponses.size()>0){
			userRegisterResponses.get(0).setMsg(SysRetCodeConstants.USERNAME_ALREADY_EXISTS.getMessage());
			userRegisterResponses.get(0).setCode(SysRetCodeConstants.USERNAME_ALREADY_EXISTS.getCode());
			return userRegisterResponses.get(0);
		}
//		判断邮箱是否存在
		userRegisterResponses=userRegisterMapper.selectUserByEmail(email);
		if(userRegisterResponses.size()>0){
			userRegisterResponses.get(0).setMsg(SysRetCodeConstants.USEREMAIL_ALREADY_EXISTS .getMessage());
			userRegisterResponses.get(0).setCode(SysRetCodeConstants.USEREMAIL_ALREADY_EXISTS .getCode());
			return userRegisterResponses.get(0);
		}

		/**
		 * 新增用户，并返回该用户的id和name，用于邮件发送
		 * 新增用户需要向member表和verify表中新增
		 * 写入密码时，要对密码进行加密，这里使用MD5加密算法, DigestUtils由spring提供
		 */
//		String password = DigestUtils.md5DigestAsHex(userRegisterRequest.getUserPwd().getBytes());
//		向member表中新增
		userRegisterMapper.insertUser(userName, password, email);
//		向verify表中新增
		String key=userRegisterRequest.getUserName()+userRegisterRequest.getUserPwd()+UUID.randomUUID();
		String uuid=DigestUtils.md5DigestAsHex(key.getBytes());
		userRegisterMapper.insertUnverifiedUser(userName,uuid);

		userRegisterResponses=userRegisterMapper.selectUserByUserName(userName);
		userRegisterResponses.get(0).setMsg(SysRetCodeConstants.SUCCESS.getMessage());
		userRegisterResponses.get(0).setCode(SysRetCodeConstants.SUCCESS.getCode());

//		发送邮件
		sendEmail(uuid,userName,email);
		
		return userRegisterResponses.get(0);
	}

	/**
	 * 给用户发送账户激活邮件
	 * @param uuid
	 * @param userName
	 * @param email
	 */
	private void sendEmail(String uuid, String userName,String email) {

//		******************简单邮件*********************************
		SimpleMailMessage mailMessage=new SimpleMailMessage();
//		邮件主题
		mailMessage.setSubject("CSMALL 用户激活");
//		发件人
		mailMessage.setFrom("cskaoyan22@163.com");
//		收件人
		mailMessage.setTo(email);


		StringBuilder stringBuilder=new StringBuilder()
				.append("http://localhost:8080/user/verify?uid=")
				.append(uuid).append("&username=").append(userName);

		String url=stringBuilder.toString();
		String html="<!DOCTYPE html>\n" +
				"<html lang=\"en\">\n" +
				"<head>\n" +
				"    <meta charset=\"UTF-8\">\n" +
				"    <title>Title</title>\n" +
				"</head>\n" +
				"<body>\n" +
				"<a href=\""+url+"\">用户激活</a>\n" +
				"</body>\n" +
				"</html>";
		String hypeLink="<a href=\""+url+"\">用户激活</a>";

//		设置邮件内容
		mailMessage.setText("尊敬的"+userName+",请激活您的账户："+url);
//		发送简单邮件
//		mailSender.send(mailMessage);


//		**********************复杂邮件**************************
		Properties properties=new Properties();
		// 身份验证
		properties.put("mail.smtp.auth","true");
		// 连接协议
		properties.put("mail.transport.protocol","smtp");
		properties.put("mail.smtp.host","smtp.qq.com");
		// 163邮件服务器可以不用指定端口号/qq邮箱的端口号为465/587
		properties.put("mail.smtp.port","587");
//		开启ssl安全连接
//		properties.put("mail.smtp.ssl.enable","true");
		// 设置是否查看debug
//		properties.put("mail.debug","true");

		Session session=Session.getInstance(properties);
		Message message=new MimeMessage(session);
//		设置复杂邮件的内容  后面的 subtype为"related" 不清楚含义
		MimeMultipart msgContent=new MimeMultipart("related");
//		设置复杂邮件的文本
		MimeBodyPart bodyPart1=new MimeBodyPart();
		MimeBodyPart bodyPart2=new MimeBodyPart();
		try {
			bodyPart1.setContent(html,"text/html;charset=utf-8");
			bodyPart2.setContent("尊敬的"+userName+", 请激活您的账户："+hypeLink,"text/html;charset=utf-8");
//			msgContent.addBodyPart(bodyPart1);
			msgContent.addBodyPart(bodyPart2);
//			设置主题
			message.setSubject("CSMALL 账户激活");
			message.setContent(msgContent);
//			也可以直接在message中存入邮件内容
//			message.setContent(html,"text/html;charset=utf-8");
//			发件人
//			message.setFrom(new InternetAddress("cskaoyan22@163.com"));
			message.setFrom(new InternetAddress("1142739329@qq.com"));
//			收件人
			message.setRecipient(Message.RecipientType.TO,new InternetAddress(email));
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		try {
//			邮差对象--邮件服务器
			Transport transport=session.getTransport();
//			transport.connect("cskaoyan22@163.com","SJPLBCHBKHIIQCLF");
			transport.connect("1142739329@qq.com","dkefdeptphxdiegd");
//			发送复杂邮件
			transport.sendMessage(message,message.getAllRecipients());
		} catch (Exception e) {
			e.printStackTrace();
		}



	}
}

package com.mall.user.dal.persistence;

import com.mall.commons.tool.tkmapper.TkMapper;
import com.mall.user.dal.entitys.UserVerify;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

public interface UserVerifyMapper extends TkMapper<UserVerify> {

	@Update("update tb_member set isverified='Y' where username=#{userName}")
	int updateMemberTable(@Param("userName") String userName);

	@Update("update tb_user_verify set is_verify='Y' where username=#{userName}")
	int updateUserVerifyTable(@Param("userName") String userName);
}
package com.mall.user.dal.persistence;

import com.mall.user.dto.UserRegisterResponse;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @auther Albert-King
 * @date 2020/8/18 6:13
 */
public interface UserRegisterMapper {

	@Insert("insert into tb_member (id,username,password,email,created,updated) value (null,#{userName},#{password},#{email},now(),now())")
	int insertUser(@Param("userName") String userName, @Param("password") String password, @Param("email") String email);

	@Insert("insert into tb_user_verify (id,username,uuid,register_date) value (null,#{userName},#{uuid},now())")
	int insertUnverifiedUser(@Param("userName") String userName, @Param("uuid") String uuid);

	@Select("select id as uid,username from tb_member where username=#{userName}")
	List<UserRegisterResponse> selectUserByUserName(@Param("userName") String userName);

	@Select("select id as uid,username from tb_member where email=#{email}")
	List<UserRegisterResponse> selectUserByEmail(@Param("email") String email);
}

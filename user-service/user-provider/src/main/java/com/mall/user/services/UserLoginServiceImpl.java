package com.mall.user.services;

import com.alibaba.fastjson.JSON;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.user.IUserLoginService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dal.persistence.UserLoginMapper;
import com.mall.user.dto.CheckAuthRequest;
import com.mall.user.dto.CheckAuthResponse;
import com.mall.user.dto.UserLoginRequest;
import com.mall.user.dto.UserLoginResponse;
import com.mall.user.utils.JwtTokenUtils;
import com.mall.user.utils.VerifyCodeUtils;
import io.netty.handler.codec.http.cookie.Cookie;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.apache.ibatis.ognl.Token;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @auther Albert-King
 * @date 2020/8/17 14:59
 */
@Slf4j
@Component
@Service
public class UserLoginServiceImpl implements IUserLoginService {
	@Autowired
	UserLoginMapper userLoginMapper;
	@Autowired
	RedissonClient redissonClient;
	private final String KAPTCHA_UUID="kaptcha_uuid";

	@Override
	public boolean captchaConfirm(String captcha,String kaptchaUuid) {
		/**
		 * 验证码实现逻辑
		 * 1、验证码存在于cookie中，存放的是验证码图片的文本数据和uuid
		 * 2、在redis中，验证码的uuid作为key，实际的验证码作为value，
		 * 需要取出redis中的验证码与前端传入的参数做对比
		 */
//		从redis中获取真实的验证码
		RBucket rBucket=redissonClient.getBucket(KAPTCHA_UUID+kaptchaUuid);
		String realCaptcha = (String) rBucket.get();
		if(captcha.equals(realCaptcha))
			return true;
		return false;
	}

	@Override
	public UserLoginResponse userLoginService(UserLoginRequest userLoginRequest) {
//		通过用户名和密码查询用户信息
		String userName=userLoginRequest.getUserName();
		String userPwd=userLoginRequest.getPassword();
		UserLoginResponse userLoginResponse = userLoginMapper.selectUserByNameAndPwd(userName, userPwd);

		if(userLoginResponse!=null)
		{
			// 验证用户是否激活
			if(userLoginMapper.selectUserByIsVerified(userLoginResponse.getId()).equals("N"))
			{
				userLoginResponse.setMsg("用户未激活");
				return userLoginResponse;
			}

//			将满足条件的用户的信息写入token
			String username = userLoginResponse.getUsername();
			Long id = userLoginResponse.getId();
			String userInfo = "{\"userName\": \"" + username + "\",\"id\": \"" + id + "\"}";
//			String userInfo=JSON.toJSON(userLoginResponse.getUsername()).toString();
			String token = JwtTokenUtils.builder().msg(userInfo).build().creatJwtToken();

			userLoginResponse.setToken(token);
			userLoginResponse.setFile("https://gper.club/server-img/avatars/000/00/00/user_origin_30.jpg?time1565591384242");
			// 成功的响应代码和信息
			userLoginResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
			userLoginResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
			return userLoginResponse;
		}
		return null;
	}

	@Override
	public CheckAuthResponse validToken(CheckAuthRequest checkAuthRequest) {
		String token = checkAuthRequest.getToken();
		String userInfo = JwtTokenUtils.builder().token(token).build().freeJwt();
		CheckAuthResponse checkAuthResponse = new CheckAuthResponse();
		checkAuthResponse.setUserinfo(userInfo);
		checkAuthResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
		checkAuthResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
		return checkAuthResponse;
	}
}

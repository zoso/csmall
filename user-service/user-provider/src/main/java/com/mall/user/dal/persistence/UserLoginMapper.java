package com.mall.user.dal.persistence;

import com.mall.user.dto.UserLoginResponse;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @auther Albert-King
 * @date 2020/8/17 15:02
 */
public interface UserLoginMapper {
	String sql="address,balance,description,email,id,phone,points,sex,state,username";
	@Select("select "+sql+" from tb_member where username=#{userName} and password=#{userPwd}")
	UserLoginResponse selectUserByNameAndPwd(@Param("userName") String userName,@Param("userPwd") String userPwd);

	@Select("select isverified from tb_member where id=#{userId}")
	String selectUserByIsVerified(@Param("userId") Long id);
}

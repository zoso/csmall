package com.mall.user.services;

import com.mall.user.IUserVerifyService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dal.entitys.UserVerify;
import com.mall.user.dal.persistence.MemberMapper;
import com.mall.user.dal.persistence.UserVerifyMapper;
import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @auther Albert-King
 * @date 2020/8/19 15:28
 */
@Component
@Service
public class UserVerifyServiceImpl implements IUserVerifyService {
	@Autowired
	MemberMapper memberMapper;
	@Autowired
	UserVerifyMapper userVerifyMapper;


	@Override
	public UserVerifyResponse userVerify(UserVerifyRequest userVerifyRequest) {
//		验证参数是否为空
		userVerifyRequest.requestCheck();

		UserVerifyResponse userVerifyResponse = new UserVerifyResponse();

//		根据用户名比对数据库verify表中的和传入的uuid是否一致
		Example example=new Example(UserVerify.class);
//		设置sql语句的条件，其中 property 表示数据库中的字段
		example.createCriteria().andEqualTo("username",userVerifyRequest.getUserName());
//		根据用户名获取查询结果
		List<UserVerify> userVerifyList=userVerifyMapper.selectByExample(example);
		if(userVerifyList.size()==0){
			userVerifyResponse.setCode(SysRetCodeConstants.USER_INFOR_INVALID.getCode());
			userVerifyResponse.setMsg(SysRetCodeConstants.USER_INFOR_INVALID.getMessage());
			return userVerifyResponse;
		}
//		判断uuid是否一致
		UserVerify userVerify = userVerifyList.get(0);
		if(!userVerifyRequest.getUuid().equals(userVerify.getUuid())){
			userVerifyResponse.setCode(SysRetCodeConstants.USER_INFOR_INVALID.getCode());
			userVerifyResponse.setMsg(SysRetCodeConstants.USER_INFOR_INVALID.getMessage());
			return userVerifyResponse;
		}
//		当uuid一致，说明用户信息未被篡改，就可以允许对数据库进行修改
//		需要修改两张表：member表中的isverified字段；user_verify表中的 is_verify字段
		int row1=userVerifyMapper.updateMemberTable(userVerifyRequest.getUserName());
		int row2=userVerifyMapper.updateUserVerifyTable(userVerifyRequest.getUserName());
//		TODO row1、row2应具有事务一致性

		userVerifyResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
		userVerifyResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
		return userVerifyResponse;
	}
}

package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IContentService;
import com.mall.shopping.IHomeService;
import com.mall.shopping.IProductCateService;
import com.mall.shopping.dto.*;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mall.shopping.IProductService;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@RestController
@RequestMapping("/shopping")
@Anoymous
public class ShoppingController {

    @Reference(timeout = 3000,check = false, retries = 0)
    IHomeService iHomeService;

    /**
     * 主页显示
     */
    @GetMapping("/homepage")
    @Anoymous
    public ResponseData homePage(){
        HomePageResponse homePageResponse = iHomeService.homepage();

        return new ResponseUtil().setData(homePageResponse.getPanelContentItemDtos());
    }

    /**
     * 导航栏显示
     */
    @GetMapping("/navigation")
    @Anoymous
    public ResponseData shoppingNavigation(){
        NavListResponse response = iHomeService.shoppingNavigation();
        return new ResponseUtil().setData(response.getPannelContentDtos());
    }

    @Reference(timeout = 3000,check = false, retries = 0)
    IProductCateService iProductCateService;

    /**
     * 列举所有商品种类
     */
    @GetMapping("/categories")
    @Anoymous
    public ResponseData shoppingCategories(AllProductCateRequest request) {
        AllProductCateResponse productCateDto = iProductCateService.getAllProductCate(request);

        return new ResponseUtil().setData(productCateDto.getProductCateDtoList());
    }

    @Reference(interfaceClass = IProductService.class)
    private IProductService productService;

    @Anoymous
    @RequestMapping("/product/{id}")
    public ResponseData product(@PathVariable("id") Long id ){
        ProductDetailResponse data =productService.getProductDetail(new ProductDetailRequest(id));
        return new ResponseUtil<>().setData(data.getProductDetailDto());
    }

    @Anoymous
    @GetMapping("/goods")
    public ResponseData goods(AllProductRequest request){
        AllProductResponse data = productService.getAllProduct(request);
        List<ProductDto> productDtoList = data.getProductDtoList();
        Long total = data.getTotal();
        ShoppingGoodsResponse goodsResponse = new ShoppingGoodsResponse();
        goodsResponse.setData(productDtoList);
        goodsResponse.setTotal(total);
        ResponseUtil<ShoppingGoodsResponse> utilData = new ResponseUtil<>();
        ResponseData<ShoppingGoodsResponse> response = utilData.setData(goodsResponse);
        return response;
    }

    @Anoymous
    @GetMapping("/recommend")
    public ResponseData recommend(){
        RecommendResponse data = productService.getRecommendGoods();
        return new ResponseUtil<>().setData(data.getPanelContentItemDtos());
    }

}

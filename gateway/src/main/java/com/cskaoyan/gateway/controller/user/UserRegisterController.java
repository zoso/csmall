package com.cskaoyan.gateway.controller.user;

import com.cskaoyan.gateway.form.userRegister.UserRegisterForm;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.user.IKaptchaService;
import com.mall.user.IUserLoginService;
import com.mall.user.UserRegisterService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.KaptchaCodeRequest;
import com.mall.user.dto.KaptchaCodeResponse;
import com.mall.user.dto.UserRegisterRequest;
import com.mall.user.dto.UserRegisterResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @auther Albert-King
 * @date 2020/8/18 6:04
 */
@RestController
@RequestMapping("user")
public class UserRegisterController {
	@Reference(timeout = 3000,check = false)
	UserRegisterService userRegisterService;

	@Anoymous
	@RequestMapping("register")
	public ResponseData userRegister(@RequestBody UserRegisterForm userRegisterForm, HttpServletRequest request){
		UserRegisterRequest userRegisterRequest=new UserRegisterRequest();
		userRegisterRequest.setEmail(userRegisterForm.getEmail());
		userRegisterRequest.setUserName(userRegisterForm.getUserName());
		userRegisterRequest.setUserPwd(userRegisterForm.getUserPwd());

		String captcha = userRegisterForm.getCaptcha();
//		获取存入cookie的验证码的uuid
		String kaptchaUuid = CookieUtil.getCookieValue(request, "kaptcha_uuid");
//		校验验证码
		boolean captchaIsRight=userRegisterService.captchaConfirm(captcha,kaptchaUuid);
		if(!captchaIsRight)
			return new ResponseUtil<>().setErrorMsg("验证码错误");

//		将用户信息写入数据库，要做用户名、邮箱是否重复的判断以及发送邮件
		UserRegisterResponse userRegisterResponse = userRegisterService.userRegister(userRegisterRequest);
		if(userRegisterResponse.getMsg().contains("已存在"))
			return new ResponseUtil<>().setErrorMsg(userRegisterResponse.getMsg());

		return new ResponseUtil<>().setData(null);
	}
}

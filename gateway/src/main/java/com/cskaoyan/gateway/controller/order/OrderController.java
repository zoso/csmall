package com.cskaoyan.gateway.controller.order;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.order.OrderCoreService;
import com.mall.order.OrderQueryService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dto.*;
import com.mall.shopping.dto.Token;
import com.mall.user.annotation.Anoymous;
import com.mall.user.intercepter.TokenIntercepter;
import io.swagger.annotations.Api;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@RestController
@RequestMapping("/shopping")
@Api(tags = "OrderController", description = "订单控制层")

public class OrderController {

    @Reference(check = false)
    private OrderCoreService orderCoreService;

    @Reference(check = false)
    private OrderQueryService orderQueryService;


    @PostMapping("/order")
    public ResponseData order(@RequestBody CreateOrderRequest request, HttpServletRequest servletRequest){
        String userInfo = (String) servletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        Token token = JSON.parseObject(userInfo,Token.class);
        request.setUserId(token.getId());
        //设置uniquekey
        request.setUniqueKey(UUID.randomUUID().toString());
        CreateOrderResponse response = orderCoreService.createOrder(request);
        if(response.getCode().equals(OrderRetCode.SUCCESS.getCode())){
            return new ResponseUtil<>().setData(response.getOrderId());
        }
        return new ResponseUtil<>().setErrorMsg(response.getMsg());

    }


    @GetMapping("/order")
    public ResponseData orderList(Integer size, Integer page, HttpServletRequest servletRequest){
        OrderListRequest request = new OrderListRequest();
        request.setPage(page);
        request.setSize(size);
        String userInfo = (String) servletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        Token token = JSON.parseObject(userInfo,Token.class);
        request.setUserId(token.getId());
        OrderListResponse response = orderQueryService.orderList(request);
        OrderListMyResponse result = new OrderListMyResponse();
        result.setData(response.getDetailInfoList());
        result.setTotal(response.getTotal());
        return new ResponseUtil().setData(result);
    }

    @GetMapping("/order/{id}")
    public ResponseData orderDetail(@PathVariable(name = "id")String id){
        OrderDetailByIdResponse response = orderQueryService.orderDetail(id);
        OrderDetailByIdMyResponse result = new OrderDetailByIdMyResponse();
        result.setUserId(response.getUserId());
        result.setGoodsList(response.getGoodsList());
        result.setOrderTotal(response.getOrderTotal());
        result.setStreetName(response.getStreetName());
        result.setUserName(response.getUserName());
        result.setTel(response.getTel());
        result.setOrderStatus(response.getOrderStatus());
        return new ResponseUtil<>().setData(result);
    }
    @DeleteMapping("/order/{id}")
    public ResponseData orderDelete(@PathVariable("id")String id){
        DeleteOrderRequest request = new DeleteOrderRequest();
        request.setOrderId(String.valueOf(id));
        DeleteOrderResponse response = orderCoreService.orderDelete(request);
        return new ResponseUtil().setData(response);

    }
    @PostMapping("/cancelOrder")
    public ResponseData cancelOrder(@RequestBody CancelOrderRequest cancelOrderRequest){
        CancelOrderResponse response = orderCoreService.cancelOrderById(cancelOrderRequest);
        return new ResponseUtil().setData(response);
    }
}
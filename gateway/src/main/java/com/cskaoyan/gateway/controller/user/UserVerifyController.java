package com.cskaoyan.gateway.controller.user;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.user.IUserVerifyService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @auther Albert-King
 * @date 2020/8/19 15:26
 */
@RestController
@RequestMapping("user")
public class UserVerifyController {

	@Reference(timeout = 3000,check = false)
	private IUserVerifyService userVerifyService;

	@Anoymous
	@RequestMapping("verify")
	public ResponseData userVerify(String uid, String username,HttpServletRequest request, HttpServletResponse response){
//		验证
		if(StringUtils.isEmpty(uid)||StringUtils.isEmpty(username)){
			return new ResponseUtil<>().setErrorMsg("注册用户/序列名不能为空");
		}

		UserVerifyRequest userVerifyRequest=new UserVerifyRequest();
		userVerifyRequest.setUserName(username);
		userVerifyRequest.setUuid(uid);

		UserVerifyResponse userVerifyResponse=userVerifyService.userVerify(userVerifyRequest);
		if(userVerifyResponse.getCode().contains(SysRetCodeConstants.SUCCESS.getCode())){
			return new ResponseUtil<>().setData(null);
		}
		return new ResponseUtil<>().setErrorMsg(userVerifyResponse.getMsg());
	}
}

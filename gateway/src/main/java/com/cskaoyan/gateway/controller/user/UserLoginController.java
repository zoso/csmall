package com.cskaoyan.gateway.controller.user;

import com.alibaba.fastjson.JSON;
import com.cskaoyan.gateway.form.userLogin.UserLoginForm;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.shopping.dto.Token;
import com.mall.user.IKaptchaService;
import com.mall.user.IUserLoginService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.KaptchaCodeResponse;
import com.mall.user.dto.UserLoginRequest;
import com.mall.user.dto.UserLoginResponse;
import com.mall.user.intercepter.TokenIntercepter;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.mall.user.intercepter.TokenIntercepter.USER_INFO_KEY;

/**
 * @auther Albert-King
 * @date 2020/8/17 13:54
 */
@RestController
@RequestMapping("user")
public class UserLoginController {

	@Reference(interfaceClass = IUserLoginService.class,timeout = 3000,check = false,retries = 0)
	private IUserLoginService userLoginService;

//	@Reference(interfaceClass = IKaptchaService.class,timeout = 3000,check = false,retries = 0)
//	private IKaptchaService kaptchaService;

	@Anoymous
	@PostMapping("login")
	public ResponseData userLoginPost(@RequestBody UserLoginForm userLoginForm,HttpServletRequest request,HttpServletResponse response){
		UserLoginRequest userLoginRequest = new UserLoginRequest();
		userLoginRequest.setPassword(userLoginForm.getUserPwd());
		userLoginRequest.setUserName(userLoginForm.getUserName());
		String captcha = userLoginForm.getCaptcha();

//		获取存入cookie的验证码的uuid
		String kaptchaUuid = CookieUtil.getCookieValue(request, "kaptcha_uuid");
//		校验验证码
		boolean captchaIsRight=userLoginService.captchaConfirm(captcha,kaptchaUuid);
		if(!captchaIsRight)
			return new ResponseUtil<>().setErrorMsg("验证码错误");

//		校验用户信息
		UserLoginResponse userLoginResponse=userLoginService.userLoginService(userLoginRequest);
		if(userLoginResponse==null)
			return new ResponseUtil<>().setErrorMsg("用户名或密码错误");
		if(userLoginResponse.getMsg().equals("用户未激活"))
			return new ResponseUtil<>().setErrorMsg("用户未激活");

//		利用工具类生成cookie以及存入响应体
		/**
		 * TokenIntercepter.ACCESS_TOKEN--access_token,作为cookie中的key
		 * userLoginResponse.getToken()-- token，存放了用户的信息，作为cookie的value
		 * uri 为 /  表示所有的请求都要携带该cookie
		 * maxAge 表示cookie的存活时间
		 */
		Cookie cookie= CookieUtil.genCookie(TokenIntercepter.ACCESS_TOKEN,userLoginResponse.getToken(),"/",6000);
//		？
		cookie.setHttpOnly(true);
//		将cookie添加到HttpServletResponse响应中，表现为 在浏览器的responseHeader中有 set-cookie 响应头
		response.addCookie(cookie);
//		CookieUtil.setCookie(response,cookie);

		return new ResponseUtil<>().setData(userLoginResponse);
	}

	@GetMapping("login")
	public ResponseData userLoginGet(HttpServletRequest request){
		String userInfo= (String) request.getAttribute(USER_INFO_KEY);
		Map map=new HashMap();
		Token token = JSON.parseObject(userInfo, Token.class);
		String file="https://gper.club/server-img/avatars/000/00/00/user_origin_30.jpg?time1565591384242";
		map.put("file",file);
		map.put("uid",token.getId());
		map.put("username",token.getUsername());
		return new ResponseUtil<>().setData(map);
	}

	@RequestMapping("loginOut")
	public ResponseData userLogOut(HttpServletRequest request,HttpServletResponse response){
		/**
		 * 用户退出的代码逻辑
		 * 本质上即删除带有用户信息的cookie
		 * 找到access_cookie，将其值替换为null即可
		 */
		Cookie[] cookies = request.getCookies();
		for(Cookie cookie:cookies){
			if (cookie.getName().equals(TokenIntercepter.ACCESS_TOKEN)){
//				将cookie置为空
				cookie.setValue(null);
//				使其立即失效
				cookie.setMaxAge(0);
				cookie.setPath("/");
				response.addCookie(cookie);
			}
		}
		return new ResponseUtil<>().setData(null);
	}
}

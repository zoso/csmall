package com.cskaoyan.gateway.controller.shopping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.shopping.ICartService;
import com.mall.shopping.dto.*;
import com.mall.user.annotation.Anoymous;
import com.mall.user.intercepter.TokenIntercepter;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/shopping")
public class CartController {

    @Reference
    ICartService iCartService;

    @GetMapping("/carts")
    public ResponseData cartList(HttpServletRequest request) {
        String userInfo = (String) request.getAttribute(TokenIntercepter.USER_INFO_KEY);
        CartListByIdRequest request2 = new CartListByIdRequest();
        Token token = JSON.parseObject(userInfo, Token.class);
        request2.setUserId(token.getId());
        List<CartProductDto> cartProductDtos = iCartService.getCartListById(request2);
        return new ResponseUtil().setData(cartProductDtos);
    }


    @PostMapping("carts")
    public ResponseData addToCart(@RequestBody AddCartRequest request) {
        iCartService.addToCart(request);
        Map map = new HashMap();
        map.put("result", "成功");
        return new ResponseUtil().setData(map);
    }

    @PutMapping("carts")
    public ResponseData updateCartNumber(@RequestBody UpdateCartNumRequest request) {
        iCartService.updateCartNum(request);
        return new ResponseUtil().setData("成功");
    }

    @DeleteMapping("carts/{userId}/{itemId}")
    public ResponseData deleteCartItem(@PathVariable("userId") Long userId, @PathVariable("itemId") Long itemId) {
        DeleteCartItemRequest request = new DeleteCartItemRequest();
        request.setItemId(itemId);
        request.setUserId(userId);
        iCartService.deleteCartItem(request);
        Map map = new HashMap();
        map.put("result", "成功");
        return new ResponseUtil().setData(map);
    }

    @PutMapping("items")
    public ResponseData checkAllCartItem(@RequestBody CheckAllItemRequest request) {
        iCartService.checkAllCartItem(request);
        return new ResponseUtil().setData("成功");
    }

    @DeleteMapping("items/{userId}")
    public ResponseData deleteCartItem(@PathVariable("userId") Long userId) {
        DeleteCheckedItemRequest request = new DeleteCheckedItemRequest();
        request.setUserId(userId);
        iCartService.deleteCheckedItem(request);
        return new ResponseUtil().setData("成功");
    }


}

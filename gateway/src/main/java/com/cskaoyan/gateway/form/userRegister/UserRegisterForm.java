package com.cskaoyan.gateway.form.userRegister;

import lombok.Data;

/**
 * @auther Albert-King
 * @date 2020/8/19 0:17
 */
@Data
public class UserRegisterForm {
	String userName;
	String userPwd;
	String email;
	String captcha;
}

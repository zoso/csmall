package com.cskaoyan.gateway.form.userLogin;

import lombok.Data;

/**
 * @auther Albert-King
 * @date 2020/8/18 10:33
 */
@Data
public class UserLoginForm {
	String userName;
	String userPwd;
	String captcha;
}

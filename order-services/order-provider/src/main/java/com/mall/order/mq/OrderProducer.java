package com.mall.order.mq;

import com.alibaba.fastjson.JSON;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;


/**
 * 发送一个订单超时取消的消息
 */
@Component
public class OrderProducer {

    @Value("${mq.producergroup}")
    private String producerGroup;
    @Value("${mq.addr}")
    private String addr;
    @Value("${mq.topic}")
    private String topic;

    DefaultMQProducer mqProducer;

    @PostConstruct
    public void init(){
        // 实例化 OrderProducer 的时候执行
        mqProducer = new DefaultMQProducer(producerGroup);
        mqProducer.setNamesrvAddr(addr);
        try {
            mqProducer.start();
        } catch (MQClientException e1) {
            e1.printStackTrace();
        }
    }

    // 发送一个订单超时的消息
    public void sendOrderOverTimeMsg(String orderId,Integer userId){

        Map map = new HashMap<String,Object>();
        map.put("orderId",orderId);
        map.put("userId",userId);

        String jsonString = JSON.toJSONString(map);

        Message message = new Message(topic, jsonString.getBytes(Charset.forName("utf-8")));

        //messageDelayLevel=  1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
        message.setDelayTimeLevel(4);
        try {
            mqProducer.send(message);
        } catch (MQClientException e) {
            e.printStackTrace();
        } catch (RemotingException e) {
            e.printStackTrace();
        } catch (MQBrokerException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

}
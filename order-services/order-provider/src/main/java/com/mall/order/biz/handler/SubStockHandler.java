package com.mall.order.biz.handler;

import com.alibaba.fastjson.JSON;
import com.mall.commons.tool.exception.BizException;
import com.mall.order.biz.context.CreateOrderContext;
import com.mall.order.biz.context.TransHandlerContext;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dal.entitys.Stock;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.StockMapper;
import com.mall.order.dto.CartProductDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Description: 扣减库存处理器
 * @Author： wz
 * @Date: 2019-09-16 00:03
 **/
@Component
@Slf4j
public class SubStockHandler extends AbstractTransHandler {

	@Autowired
	private StockMapper stockMapper;

	@Override
	public boolean isAsync() {
		return false;
	}

	@Override
	@Transactional
	public boolean handle(TransHandlerContext context) {

		CreateOrderContext createOrderContext = (CreateOrderContext) context;
		List<CartProductDto> dtoList = createOrderContext.getCartProductDtoList();
		List<Long> productIds = createOrderContext.getBuyProductIds();
		if(CollectionUtils.isEmpty(productIds)){
			//方法一：for循环
			/*for (CartProductDto cartProductDto : dtoList) {
				productIds.add(cartProductDto.getProductId());
			}*/
			//方法二：λ表达式：获取到流，
			productIds = dtoList.stream().map(u -> u.getProductId()).collect(Collectors.toList());
		}
		//排序
		productIds.sort(Long::compareTo);

		//锁定库存
		List<Stock> stockList = stockMapper.findStocksForUpdate(productIds);
		if(CollectionUtils.isEmpty(stockList)){
			throw new BizException(OrderRetCode.SYSTEM_ERROR .getCode(),"库存未初始化");
		}

		//由于有的商品在库存表里没有，会报错
		if(stockList.size() != productIds.size()){
			throw new BizException(OrderRetCode.SYSTEM_ERROR .getCode(),"部分商品库存未初始化");
		}

		//扣减库存
		for (CartProductDto cartProductDto : dtoList) {
			Long productId = cartProductDto.getProductId();
			Long productNum = cartProductDto.getProductNum();

			//productNum不能超出限购数量
			Stock stock1 = stockMapper.selectStock(cartProductDto.getProductId());
			if(stock1.getRestrictCount().longValue() < productNum.longValue()){
				throw new BizException(OrderRetCode.SYSTEM_ERROR .getCode(),"购买数量超过限购");
			}

			Stock stock = new Stock();
			stock.setItemId(productId);
			stock.setLockCount(productNum.intValue());
			stock.setStockCount(-productNum);
			stockMapper.updateStock(stock);
		}
		return true;
	}
}
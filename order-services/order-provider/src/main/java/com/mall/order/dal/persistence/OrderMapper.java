package com.mall.order.dal.persistence;

import com.mall.commons.tool.tkmapper.TkMapper;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dto.OrderDetailByIdResponse;
import com.mall.order.dto.OrderListRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper extends TkMapper<Order> {
    Long countAll();


    OrderDetailByIdResponse selectByOrderId(String orderId);


    List<Order> selectByUserId(Long userId);

    void cancelOrder(String orderId);
}
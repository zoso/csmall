package com.mall.order.mq;

import com.alibaba.fastjson.JSON;
import com.mall.order.OrderCoreService;
import com.mall.order.constants.OrderConstants;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dal.persistence.OrderShippingMapper;
import com.mall.order.dal.persistence.StockMapper;
import com.mall.order.dto.CancelOrderRequest;
import com.mall.shopping.ICartService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;


@Component
public class OrderConsumer {

    @Autowired
    private OrderMapper orderMapper;

    @Reference
    private OrderCoreService orderCoreService;


    private DefaultMQPushConsumer consumer;

    @Value("${mq.addr}")
    private String addr;
    @Value("${mq.topic}")
    private String topic;


    @PostConstruct
    public void init(){

        consumer = new DefaultMQPushConsumer("consumer_group");

        // 设置注册中心的地址
        consumer.setNamesrvAddr(addr);

        // 订阅topic
        try {
            consumer.subscribe(topic,"*");
        } catch (MQClientException e) {
            e.printStackTrace();
        }
        // 注册消息监听器
        consumer.registerMessageListener(new MessageListenerConcurrently() {

            // 消息消费者收到了消息之后调用
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {

                // 收到延迟消息后
                MessageExt messageExt = msgs.get(0);
                byte[] body = messageExt.getBody();
                String bodyStr = new String(body);

                Map<String,Object> map  = JSON.parseObject(bodyStr,Map.class);
                String orderId = (String) map.get("orderId");
                Integer userId = (Integer) map.get("userId");

                // 处理取消订单
                Order order = orderMapper.selectByPrimaryKey(orderId);
                if (order.getStatus() == OrderConstants.ORDER_STATUS_INIT) {
                    order.setStatus(OrderConstants.ORDER_STATUS_TRANSACTION_CANCEL);
                    orderMapper.updateByPrimaryKeySelective(order);
                    CancelOrderRequest cancelOrderRequest = new CancelOrderRequest();
                    cancelOrderRequest.setOrderId(orderId);
                    orderCoreService.cancelOrderById(cancelOrderRequest);
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                // 假如在这里消费失败的话，rocketmq默认会去重试，重试16次
            }
        });


        try {
            consumer.start();
        } catch (MQClientException e) {
            e.printStackTrace();
        }
        System.out.println("消息消费者启动成功！！！");
    }
}
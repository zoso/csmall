package com.mall.order.services;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mall.commons.tool.exception.BizException;
import com.mall.order.OrderQueryService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.entitys.OrderShipping;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dal.persistence.OrderShippingMapper;
import com.mall.order.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *  ciggar
 * create-date: 2019/7/30-上午10:04
 */
@Slf4j
@Component
@Service
public class OrderQueryServiceImpl implements OrderQueryService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private OrderShippingMapper orderShippingMapper;


    /**
     * 查看用户所有订单
     * @param request
     * @return
     */
    @Override
    public OrderListResponse orderList(OrderListRequest request) {
        OrderListResponse response = new OrderListResponse();
        List<OrderDetailInfo> list = new ArrayList<>();

        Example example = new Example(Order.class);
        if(request.getSort() != null){
            example.setOrderByClause(request.getSort());
        }
        PageHelper.startPage(request.getPage(),request.getSize());

        List<Order> orders = orderMapper.selectByUserId(request.getUserId());

        for (Order order : orders) {
            OrderDetailInfo orderDetailInfo = new OrderDetailInfo();
            orderDetailInfo.setOrderId(order.getOrderId());
            orderDetailInfo.setPayment(order.getPayment());
            orderDetailInfo.setPaymentType(order.getPaymentType());
            orderDetailInfo.setPostFee(order.getPostFee());
            orderDetailInfo.setStatus(order.getStatus());
            orderDetailInfo.setCreateTime(order.getCreateTime());
            orderDetailInfo.setUpdateTime(order.getUpdateTime());
            orderDetailInfo.setPaymentTime(order.getPaymentTime());
            orderDetailInfo.setConsignTime(order.getConsignTime());
            orderDetailInfo.setEndTime(order.getEndTime());
            orderDetailInfo.setCloseTime(order.getCloseTime());
            orderDetailInfo.setShippingName(order.getShippingName());
            orderDetailInfo.setShippingCode(order.getShippingCode());
            orderDetailInfo.setUserId(order.getUserId());
            orderDetailInfo.setBuyerMessage(order.getBuyerMessage());
            orderDetailInfo.setBuyerNick(order.getBuyerNick());
            orderDetailInfo.setBuyerComment(order.getBuyerComment());

            //获得orderItemDto部分
            List<OrderItemDto> orderItems = orderItemMapper.selectList(order.getOrderId());
            orderDetailInfo.setOrderItemDto(orderItems);

            //获得ordershipping部分
            OrderShipping orderShipping = orderShippingMapper.selectByPrimaryKey(order.getOrderId());
            OrderShippingDto orderShippingDto = new OrderShippingDto();
            orderShippingDto.setOrderId(orderShipping.getOrderId());
            orderShippingDto.setReceiverName(orderShipping.getReceiverName());
            orderShippingDto.setReceiverPhone(orderShipping.getReceiverPhone());
            orderShippingDto.setReceiverMobile(orderShipping.getReceiverMobile());
            orderShippingDto.setReceiverState(orderShipping.getReceiverState());
            orderShippingDto.setReceiverCity(orderShipping.getReceiverCity());
            orderShippingDto.setReceiverDistrict(orderShipping.getReceiverDistrict());
            orderShippingDto.setReceiverAddress(orderShipping.getReceiverAddress());
            orderShippingDto.setReceiverZip(orderShipping.getReceiverZip());
            orderDetailInfo.setOrderShippingDto(orderShippingDto);
            list.add(orderDetailInfo);
        }
        long total = list.size();
        response.setDetailInfoList(list);
        response.setTotal(total);
        return response;
    }

    /**
     * 查看订单详情
     * @param orderId
     * @param orderId
     * @return
     */
    @Override
    public OrderDetailByIdResponse orderDetail(String orderId) {
        OrderDetailByIdResponse orderDetail = orderMapper.selectByOrderId(orderId);
        OrderShipping orderShipping = orderShippingMapper.selectByPrimaryKey(orderId);
        List<OrderItemDto> list = orderItemMapper.selectList(orderId);
        BigDecimal sum = new BigDecimal(0);
        for (OrderItemDto orderItemDto : list) {
            BigDecimal num = new BigDecimal(orderItemDto.getNum());
            sum = sum.add(orderItemDto.getPrice().multiply(num) );
        }
        orderDetail.setUserName(orderShipping.getReceiverName());
        orderDetail.setOrderTotal(sum);
        orderDetail.setGoodsList(list);
        return orderDetail;
    }



}

package com.mall.order.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderDetailByIdMyResponse{
    private String userName;

    private BigDecimal orderTotal;

    private Long userId;

    private List<OrderItemDto> goodsList;

    private String tel;

    private String streetName;

    private Integer orderStatus;



}

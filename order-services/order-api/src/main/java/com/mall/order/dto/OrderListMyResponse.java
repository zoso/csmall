package com.mall.order.dto;

import lombok.Data;

import java.util.List;

@Data
public class OrderListMyResponse {
    private List<OrderDetailInfo> data;

    /**
     * 总记录数
     */
    private Long total;
}

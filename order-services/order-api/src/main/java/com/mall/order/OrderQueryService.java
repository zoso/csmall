package com.mall.order;

import com.mall.order.dto.OrderDetailByIdResponse;
import com.mall.order.dto.OrderListRequest;
import com.mall.order.dto.OrderListResponse;

/**
 *  ciggar
 * create-date: 2019/7/30-上午10:01
 */
public interface OrderQueryService {


    OrderListResponse orderList(OrderListRequest request);


    OrderDetailByIdResponse orderDetail(String orderId);
}
